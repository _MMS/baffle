package com.mms.maze.entity;

import java.util.ArrayList;
import java.util.List;

import com.mms.maze.Controls;
import com.mms.maze.Input;
import com.mms.maze.graphics.Bitmap;
import com.mms.maze.graphics.Screen;
import com.mms.maze.graphics.Sprite;
import com.mms.maze.level.Level;
import com.mms.maze.level.tile.Tile;

public class Player{
	
	private int x, y;
	public int direction, speed;
	private Level level;
	private Hitbox hitbox;
	private Input input;
	private int anim = 1;
	private int animDelay = 10, animCounter = 0;
	private boolean animate = false;
	public List<Bitmap> sprites = new ArrayList<Bitmap>();
	protected int dirs, anims;
	
	public int progression = 1;
	
	public Player(Input input) {
		this.input = input;
		this.speed = 1;
		hitbox = new Hitbox(this, 4, 9, 8, 6);
		setupSprites();
	}
	
	/**
	 * Setup the player's sprites
	 */
	private void setupSprites() {
		dirs = 3;
		anims = 3;
		sprites.add(Sprite.player_back_1_sprite);
		sprites.add(Sprite.player_side_1_sprite);
		sprites.add(Sprite.player_forward_1_sprite);
		sprites.add(Sprite.player_back_2_sprite);
		sprites.add(Sprite.player_side_2_sprite);
		sprites.add(Sprite.player_forward_2_sprite);
		sprites.add(Sprite.player_back_3_sprite);
		sprites.add(Sprite.player_side_3_sprite);
		sprites.add(Sprite.player_forward_3_sprite);
	}
	
	/**
	 * Move the player to a new level
	 * @param level The new level
	 * @param x The player's new x position
	 * @param y The player's new y position
	 */
	public void newLevel(Level level, int x, int y){
		this.level = level;
		setPosition(x, y);
	}
	
	/**
	 * Set the player's position in the level
	 * @param x
	 * @param y
	 */
	public void setPosition(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Set the direction the player is facing
	 * @param dir
	 */
	public void setDirection(int dir){
		this.direction = dir;
	}
	
	/**
	 * Gets the direction the player is facing
	 * @return
	 */
	public int getDirection(){
		return direction;
	}
	
	/**
	 * Gets the player's current level
	 * @return
	 */
	public Level getLevel(){
		return level;
	}
	
	/**
	 * Gets the player's hitbox
	 * @return
	 */
	public Hitbox getHitbox(){
		return hitbox;
	}
	
	/**
	 * Moves the player
	 * @param xa
	 * @param ya
	 */
	private void move(int xa, int ya){
		if(xa != 0 && ya != 0){
			move(xa, 0);
			move(0, ya);
			return;
		}
		
		if(xa < 0) direction = 3;
		else if(xa > 0) direction = 1;
		else if(ya < 0) direction = 0;
		else if(ya > 0) direction = 2;
		
		if(!willCollide(xa, ya)){
			x += xa;
			y += ya;
		}
	}
	
	/**
	 * Will the player collide if they move
	 * @param xa X acceleration
	 * @param ya Y acceleration
	 * @return
	 */
	private boolean willCollide(int xa, int ya){
		if(x + xa < 0) return true;
		else if(y + ya < 0) return true;
		else if(x + 15 + xa > level.getWidth() << 4) return true;
		else if(y + 15 + ya > level.getHeight() << 4) return true;
		
		if(Tile.getTile(level.getTile((hitbox.getX() + xa) >> 4, (hitbox.getY() + ya) >> 4)).isSolid()) return true;
		else if(Tile.getTile(level.getTile((hitbox.getX() + hitbox.getWidth() + xa) >> 4, (hitbox.getY() + ya) >> 4)).isSolid()) return true;
		else if(Tile.getTile(level.getTile((hitbox.getX() + xa) >> 4, (hitbox.getY() + hitbox.getHeight() + ya) >> 4)).isSolid()) return true;
		else if(Tile.getTile(level.getTile((hitbox.getX() + hitbox.getWidth() + xa) >> 4, (hitbox.getY() + hitbox.getHeight() + ya) >> 4)).isSolid()) return true;
		return false;
	}
	
	/**
	 * Update player logic
	 */
	public void tick() {
		int xa = 0, ya = 0;
		if(input.isKeyPressed(Controls.FORWARD)) ya -= speed;
		if(input.isKeyPressed(Controls.BACK)) ya += speed;
		if(input.isKeyPressed(Controls.LEFT)) xa -= speed;
		if(input.isKeyPressed(Controls.RIGHT)) xa += speed;
		move(xa, ya);
		
		if(xa != 0 || ya != 0){
			if(animCounter < animDelay){
				animCounter++;
			}else{
				animCounter = 0;
				if(anim == 1) anim = 2;
				else if(anim == 2) anim = 1;
			}
			animate = true;
		}else{
			animate = false;
		}
	}
	
	/**
	 * Gets the player's X position
	 * @return
	 */
	public int getX(){
		return x;
	}
	
	/**
	 * Gets the player's Y position
	 * @return
	 */
	public int getY(){
		return y;
	}

	/**
	 * Draw the player
	 * @param screen
	 */
	public void render(Screen screen) {
		boolean xflip = false;
		int animStrip = direction;
		if(animStrip == 3){
			animStrip = 1;
			xflip = true;
		}
		screen.drawMap(sprites.get(animStrip + (animate ? anim : 0) * 3), x, y, xflip ? Screen.BIT_FLIP_X : Screen.BIT_FLIP_NONE);
	}
}