package com.mms.maze.entity;

public class Hitbox{
	
	private Player owner;
	private int xo, yo, width, height;
	
	public Hitbox(Player owner, int xo, int yo, int width, int height){
		this.owner = owner;
		this.xo = xo;
		this.yo = yo;
		this.width = width;
		this.height = height;
	}
	
	/**
	 * Get the x position of the hitbox
	 * @return
	 */
	public int getX(){
		return owner.getX() + xo;
	}
	
	/**
	 * Get the y position of the hitbox
	 * @return
	 */
	public int getY(){
		return owner.getY() + yo;
	}
	
	/**
	 * Get the width of the hitbox
	 * @return
	 */
	public int getWidth(){
		return width;
	}
	
	/**
	 * Get the height of the hitbox
	 * @return
	 */
	public int getHeight(){
		return height;
	}
	
	/**
	 * Check whether a point intersects with the hixbox;
	 * @param x X of the point to check
	 * @param y Y of the point to check
	 * @return
	 */
	public boolean intersects(int x, int y){
		int xMin = getX();
		int xMax = xMin + width - 1;
		int yMin = getY();
		int yMax = yMin + height - 1;
		return x >= xMin && x <= xMax && y >= yMin && y <= yMax;
	}
	
}