package com.mms.maze.level.tile;

import com.mms.maze.graphics.Bitmap;

public class CaveDoorTile extends ActionTile{

	protected CaveDoorTile(Bitmap image, String levelToken) {
		super(image, levelToken, false);
	}

	@Override
	public void action() {
		owner.getHandler().loadLevel("cave");
	}

}