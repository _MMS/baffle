package com.mms.maze.level.tile;

import com.mms.maze.graphics.Bitmap;

public class ProgressTile extends ActionTile{

	protected ProgressTile(Bitmap image, String levelToken) {
		super(image, levelToken, false);
	}

	@Override
	public void action() {
		owner.getHandler().nextLevel();
	}

}