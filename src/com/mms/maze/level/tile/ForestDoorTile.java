package com.mms.maze.level.tile;

import com.mms.maze.graphics.Bitmap;

public class ForestDoorTile extends ActionTile{

	protected ForestDoorTile(Bitmap image, String levelToken) {
		super(image, levelToken, false);
	}

	@Override
	public void action() {
		owner.getHandler().loadLevel("forest");
	}
	
}