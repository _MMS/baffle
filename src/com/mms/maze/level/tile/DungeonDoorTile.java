package com.mms.maze.level.tile;

import com.mms.maze.graphics.Bitmap;

public class DungeonDoorTile extends ActionTile{

	protected DungeonDoorTile(Bitmap image, String levelToken) {
		super(image, levelToken, false);
	}

	@Override
	public void action() {
		owner.getHandler().loadLevel("dungeon");
	}
	
}