package com.mms.maze.level.tile;

import java.util.ArrayList;
import java.util.List;

import com.mms.maze.graphics.Bitmap;
import com.mms.maze.graphics.Screen;
import com.mms.maze.graphics.Sprite;
import com.mms.maze.level.Level;

@SuppressWarnings("unused")
public abstract class Tile{
	
	protected String levelToken;
	protected int id;
	protected Bitmap image;
	protected boolean solid;
	protected Level owner;
	
	private static List<Tile> tiles = new ArrayList<Tile>();
	
	private static Tile void_tile = new SolidTile(new Bitmap(16, 16, 0xffffff), "_");
	private static Tile level_incomplete = new SolidTile(Sprite.level_incomplete_sprite, "in");
	private static Tile level_complete = new SolidTile(Sprite.level_complete_sprite, "co");
	private static Tile level_wall = new SolidTile(Sprite.level_wall_sprite, "wa");
	private static Tile level_door_cave = new CaveDoorTile(Sprite.level_door_sprite, "cd");
	private static Tile level_door_forest = new ForestDoorTile(Sprite.level_door_sprite, "fd");
	private static Tile level_door_dungeon = new DungeonDoorTile(Sprite.level_door_sprite, "dd");
	private static Tile level_progress = new ProgressTile(Sprite.level_next_sprite, "ne");
	private static Tile cave_start = new StartTile(Sprite.cave_path_vertical_sprite, "cs");
	private static Tile cave_wall_1 = new SolidTile(Sprite.cave_wall_1_sprite, "cw1");
	private static Tile cave_wall_2 = new SolidTile(Sprite.cave_wall_2_sprite, "cw2");
	private static Tile cave_wall_3 = new SolidTile(Sprite.cave_wall_3_sprite, "cw3");
	private static Tile cave_floor_vertical = new BasicTile(Sprite.cave_path_vertical_sprite, "cpv");
	private static Tile cave_floor_horizontal = new BasicTile(Sprite.cave_path_horizontal_sprite, "cph");
	private static Tile cave_floor_corner1 = new BasicTile(Sprite.cave_path_corner_1_sprite, "cc1");
	private static Tile cave_floor_corner2 = new BasicTile(Sprite.cave_path_corner_2_sprite, "cc2");
	private static Tile cave_floor_corner3 = new BasicTile(Sprite.cave_path_corner_3_sprite, "cc3");
	private static Tile cave_floor_corner4 = new BasicTile(Sprite.cave_path_corner_4_sprite, "cc4");
	private static Tile forest_wall = new SolidTile(Sprite.forest_wall_sprite, "fw");
	private static Tile forest_floor = new BasicTile(Sprite.forest_floor_sprite, "ff");
	private static Tile forest_start = new StartTile(Sprite.forest_floor_sprite, "fs");
	private static Tile dungeon_wall = new SolidTile(Sprite.dungeon_wall_sprite, "dw");
	private static Tile dungeon_floor = new BasicTile(Sprite.dungeon_floor_sprite, "df");
	private static Tile dungeon_start = new StartTile(Sprite.dungeon_floor_sprite, "ds");
	
	protected Tile(Bitmap image, String levelToken, boolean solid){
		if(!tiles.contains(this)){
			tiles.add(this);
			this.id = tiles.indexOf(this);
			this.image = image;
			this.levelToken = levelToken;
			this.solid = solid;
		}
	}
	
	/**
	 * Gets the id of the tile
	 * @return
	 */
	public int getId(){
		return id;
	}
	
	/**
	 * Gets the level token of the tile
	 * @return
	 */
	public String getLevelToken(){
		return levelToken;
	}
	
	/**
	 * Whether or not the tile is solid
	 */
	public boolean isSolid(){
		return solid;
	}

	/**
	 * Sets the owner of the level
	 * @param owner
	 */
	public void setOwner(Level owner){
		this.owner = owner;
	}
	
	/**
	 * Get a tile by its id
	 * @param id
	 * @return
	 */
	public static Tile getTile(int id){
		return tiles.get(id);
	}
	
	/**
	 * Get a tile by its level token
	 * @param levelToken
	 * @return
	 */
	public static Tile getTile(String levelToken){
		for(Tile t : tiles){
			if(t.levelToken.equals(levelToken)) return t;
		}
		return tiles.get(0);
	}
	
	/**
	 * Render the tile to the screen
	 * @param screen
	 * @param x
	 * @param y
	 */
	public void render(Screen screen, int x, int y){
		screen.drawMap(image, x, y);
	}
	
}