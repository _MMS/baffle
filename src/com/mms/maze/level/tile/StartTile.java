package com.mms.maze.level.tile;

import com.mms.maze.graphics.Bitmap;

public class StartTile extends Tile{

	protected StartTile(Bitmap image, String levelToken) {
		super(image, levelToken, false);
	}

}