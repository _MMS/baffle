package com.mms.maze.level.tile;

import com.mms.maze.graphics.Bitmap;

public class BasicTile extends Tile{

	protected BasicTile(Bitmap image, String levelToken) {
		super(image, levelToken, false);
	}

}