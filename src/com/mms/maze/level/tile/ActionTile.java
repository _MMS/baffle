package com.mms.maze.level.tile;

import com.mms.maze.graphics.Bitmap;

public abstract class ActionTile extends Tile{

	protected ActionTile(Bitmap image, String levelToken, boolean solid) {
		super(image, levelToken, solid);
	}
	
	public void tick(int x, int y){
		if(owner == null) return;
		
		int x0 = owner.getPlayer().getHitbox().getX();
		int x1 = owner.getPlayer().getHitbox().getX() + 15;
		int y0 = owner.getPlayer().getHitbox().getY();
		int y1 = owner.getPlayer().getHitbox().getY() + 15;
		
		if(x >= x0 && x <= x1 && y >= y0 && y <= y1) action();
		else if(x + 15 >= x0 && x + 15 <= x1 && y >= y0 && y <= y1) action();
		else if(x >= x0 && x <= x1 && y + 15 >= y0 && y + 15 <= y1) action();
		else if(x + 15 >= x0 && x + 15 <= x1 && y + 15 >= y0 && y + 15 <= y1) action();
	}
	
	public abstract void action();

}