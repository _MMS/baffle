package com.mms.maze.level.tile;

import com.mms.maze.graphics.Bitmap;

public class SolidTile extends Tile{
	
	public SolidTile(Bitmap image, String levelToken){
		super(image, levelToken, true);
	}
	
}
