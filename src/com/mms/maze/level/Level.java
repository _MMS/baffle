package com.mms.maze.level;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

import com.mms.maze.entity.Player;
import com.mms.maze.graphics.Screen;
import com.mms.maze.level.tile.ActionTile;
import com.mms.maze.level.tile.StartTile;
import com.mms.maze.level.tile.Tile;

public class Level{
	
	protected int startX = 0, startY = 0;
	private int[] tiles;
	private int width, height;
	private LevelHandler handler;
	
	/**
	 * Create a blank level
	 * @param width Width of the level
	 * @param height Height of the level
	 */
	public Level(int width, int height, LevelHandler handler){
		this.width = width;
		this.height = height;
		this.handler = handler;
		tiles = new int[width * height];
		for(int i = 0; i < tiles.length; i++){
			tiles[i] = 0;
		}
	}
	
	/**
	 * Create a level from a file
	 * @param path Path of the file
	 */
	public Level(String path, LevelHandler handler){
		this.handler = handler;
		loadLevel(path);
	}
	
	/**
	 * Load level from file
	 * @param path
	 */
	private void loadLevel(String path){
		try(BufferedReader reader = new BufferedReader(new FileReader(new File(path)))){
			String line;
			StringTokenizer tokenizer;
			
			int y = -1;
			int x = 0;
			this.width = 0;
			this.height = 0;
			
			while((line = reader.readLine()) != null){
				tokenizer = new StringTokenizer(line, " ");
				while(tokenizer.hasMoreTokens()){
					if(y < 0){
						if(width == 0) width = Integer.parseInt(tokenizer.nextToken());
						else if(height == 0) height = Integer.parseInt(tokenizer.nextToken());
					}else{
						if(y == 0 && x == 0) tiles = new int[width * height];
						String token = tokenizer.nextToken();
						tiles[x + y * width] = Tile.getTile(token).getId();
						Tile.getTile(token).setOwner(this);
						if(Tile.getTile(token) instanceof StartTile){
							startX = x << 4;
							startY = y << 4;
						}
						x++;
						if(x >= width) x = 0;
					}
				}
				y++;
				if(y >= height) y = 0;
			}
		}catch(FileNotFoundException e){
			System.err.println("File not found at " + path);
			return;
		}catch(IOException e){
			System.err.println("Failed to load file from " + path);
			return;
		}
	}
	
	/**
	 * Gets the player of the level
	 * @return
	 */
	public Player getPlayer(){
		return handler.getGame().getPlayer();
	}
	
	/**
	 * Gets the handler of the level
	 * @return
	 */
	public LevelHandler getHandler(){
		return handler;
	}
	
	/**
	 * Gets a tile
	 * @param x X of the tile
	 * @param y Y of the tile
	 * @return Tile's id
	 */
	public int getTile(int x, int y){
		try{
			return tiles[x + y * width];
		}catch(ArrayIndexOutOfBoundsException e){
			return 0;
		}
	}
	
	/**
	 * Gets the width of the level in tiles
	 * @return
	 */
	public int getWidth(){
		return width;
	}
	
	/**
	 * Gets the height of the level in tiles
	 * @return
	 */
	public int getHeight(){
		return height;
	}
	
	/**
	 * Update the tiles that indicate progression in the level
	 * If progression is maxed and the player hasn't already won using the save, win the game
	 */
	public void updateProgression(){
		if(getPlayer().progression % 2 == 0) tiles[3 + 1 * width] = Tile.getTile("co").getId();
		else tiles[3 + 1 * width] = Tile.getTile("in").getId();
		if(getPlayer().progression % 3 == 0) tiles[7 + 1 * width] = Tile.getTile("co").getId();
		else tiles[7 + 1 * width] = Tile.getTile("in").getId();
		if(getPlayer().progression % 5 == 0) tiles[11 + 1 * width] = Tile.getTile("co").getId();
		else tiles[11 + 1 * width] = Tile.getTile("in").getId();
		
		if(getPlayer().progression % 2 == 0 && getPlayer().progression % 3 == 0 && getPlayer().progression % 5 == 0){
			handler.getGame().win();
		}
	}
	
	/**
	 * Update level logic
	 */
	public void tick(){
		for(int y = 0; y < height; y++){
			for(int x = 0; x < width; x++){
				if(Tile.getTile(tiles[x + y * width]) instanceof ActionTile){
					ActionTile tile = (ActionTile) Tile.getTile(tiles[x + y * width]);
					tile.tick(x << 4, y << 4);
				}
			}
		}
	}
	
	/**
	 * Render level
	 * @param screen Screen to render with
	 * @param xo X offset
	 * @param yo Y offset
	 */
	public void render(Screen screen, int xo, int yo){
		int xOffset = xo - screen.getWidth() / 2 + 8;
		int yOffset = yo - screen.getHeight() / 2 + 8;
		
		if(width << 4 < screen.getWidth()) xOffset = -(screen.getWidth() / 2) + ((width << 4) / 2);
		else if(xOffset < 0) xOffset = 0;
		else if(xOffset > (width << 4) - screen.getWidth()) xOffset = (width << 4) - screen.getWidth();
		
		if(height << 4 < screen.getHeight()) yOffset = -(screen.getHeight() / 2) + ((height << 4) / 2);
		else if(yOffset < 0) yOffset = 0;
		else if(yOffset > (height << 4) - screen.getHeight()) yOffset = (height << 4) - screen.getHeight();
		
		screen.setOffset(xOffset, yOffset);
		
		int x0 = xOffset >> 4;
		int x1 = x0 + (screen.getWidth() >> 4) + 1;
		int y0 = yOffset >> 4;
		int y1 = y0 + (screen.getHeight() >> 4) + 2;
		
		for(int y = y0; y < y1; y++){
			for(int x = x0; x < x1; x++){
				if(y >= 0 && x >= 0 && y < height && x < width){
					Tile.getTile(tiles[x + y * width]).render(screen, x << 4, y << 4);
				}			
			}
		}
	}
}