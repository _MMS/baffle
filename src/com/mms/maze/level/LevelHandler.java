package com.mms.maze.level;

import java.util.HashMap;

import com.mms.maze.Game;

public class LevelHandler{
	
	private Game game;
	private int currentLevel = 0;
	private Level[] levels = new Level[10];
	private String[] levelPaths = {
			"res/lvl/hub.lvl",
			"res/lvl/cave/1.lvl",
			"res/lvl/cave/2.lvl",
			"res/lvl/forest/1.lvl",
			"res/lvl/forest/2.lvl",
			"res/lvl/forest/3.lvl",
			"res/lvl/dungeon/1.lvl",
			"res/lvl/dungeon/2.lvl",
			"res/lvl/dungeon/3.lvl",
			"res/lvl/dungeon/4.lvl",
	};
	private HashMap<String, Integer> levelPointers = new HashMap<String, Integer>();
	
	public LevelHandler(Game game){
		this.game = game;
		levelPointers.put("hub", 0);
		levelPointers.put("cave", 1);
		levelPointers.put("forest", 3);
		levelPointers.put("dungeon", 6);
	}
	
	/**
	 * Gets the current loaded level
	 * @return
	 */
	public Level getCurrentLevel(){
		if(currentLevel < 0 || currentLevel >= levels.length) return null;
		if(levels[currentLevel] != null){
			return levels[currentLevel];
		}else{
			levels[currentLevel] = new Level(levelPaths[currentLevel], this);
			return getCurrentLevel();
		}
	}
	
	/**
	 * Gets the id of the current loaded level
	 * @return
	 */
	public int getCurrentLevelId(){
		return currentLevel;
	}
	
	/**
	 * Go to the next level
	 */
	public void nextLevel(){
		if(getCurrentLevel() == null) return;
		switch(currentLevel){
		case 2:
			currentLevel = 0;
			if(game.getPlayer().progression % 2 != 0) game.getPlayer().progression *= 2;
			break;
		case 5:
			currentLevel = 0;
			if(game.getPlayer().progression % 3 != 0) game.getPlayer().progression *= 3;
			break;
		case 9:
			currentLevel = 0;
			if(game.getPlayer().progression % 5 != 0) game.getPlayer().progression *= 5;
			break;
		default:
			currentLevel++;
			break;
		}
		loadLevel(currentLevel);
	}
	
	/**
	 * Load level with a name pointer
	 * @param name
	 */
	public void loadLevel(String name){
		if(levelPointers.containsKey(name)){
			currentLevel = levelPointers.get(name);
			game.getPlayer().newLevel(getCurrentLevel(), getCurrentLevel().startX, getCurrentLevel().startY);
			if(currentLevel == 0) getCurrentLevel().updateProgression();
		}else{
			System.err.println("Could not find level with pointer \'" + name + "\'");
		}
	}
	
	/**
	 * Load a level with its id
	 * @param id
	 */
	public void loadLevel(int id){
		currentLevel = id;
		game.getPlayer().newLevel(getCurrentLevel(), getCurrentLevel().startX, getCurrentLevel().startY);
		if(currentLevel == 0) getCurrentLevel().updateProgression();
	}
	
	/**
	 * Get the game
	 * @return
	 */
	public Game getGame(){
		return game;
	}
}