package com.mms.maze;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import javax.swing.JFrame;

import com.mms.maze.Game.GameState;
import com.mms.maze.graphics.Screen;
import com.mms.maze.graphics.Sprite;
import com.mms.maze.graphics.Text;

public class Container extends Canvas implements Runnable{
	
	private static final long serialVersionUID = 1L;
	//Size variables
	private static final int HEIGHT = 180;
	private static final int WIDTH = HEIGHT / 3 * 4;
	private static final int SCALE = 3;
	//Frame display variables
	private static final String TITLE = "Baffle | v1.0";
	
	//Runtime variables
	private Thread thread;
	private boolean running = false;
	private JFrame frame;
	
	//Game variables
	private Game game;
	private Screen screen;
	private Input input;
	private BufferedImage image;
	private int[] pixels;
	
	/**
	 * Instantiates the container, by extension the game.
	 */
	public Container(){
		//set size of window
		Dimension size = new Dimension(WIDTH * SCALE, HEIGHT * SCALE);
		setSize(size);
		setMinimumSize(size);
		setMaximumSize(size);
		setPreferredSize(size);
		
		//set up window
		frame = new JFrame(TITLE);
		frame.setResizable(false); 
		frame.add(this);
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		//set up game variables
		input = new Input();
		screen = new Screen(WIDTH, HEIGHT);
		game = new Game(this, screen);
		game.setState(GameState.STARTING);
		image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
		Sprite.colorSprites();
		
		//set up listeners
		addFocusListener(input);
		addKeyListener(input);
		addMouseListener(input);
		addMouseMotionListener(input);
		
		System.out.println("Resolution: " + WIDTH + "x" + HEIGHT);
		//start game thread
		this.start();
	}
	
	/**
	 * Start the game thread.
	 */
	public synchronized void start(){
		if(running) return;
		running = true;
		thread = new Thread(this);
		thread.start();
	}
	
	/**
	 * Stop the game thread.
	 */
	public synchronized void stop(){
		if(!running) return;
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * The game loop
	 */
	@Override
	public void run(){
		//Tick limiter variables
		long lastTime = System.nanoTime();
		double delta = 0D;
		double secondsPerTick = 1 / 60.0D;
		int frames = 0;
		int ticks = 0;
		
		//game loop
		while(running){
			//all this stuff makes sure the game "ticks" and "renders" a max of 60 times per second
			long now = System.nanoTime();
			long time = now - lastTime;
			lastTime = now;
			
			if(time < 0) time = 0;
			if(time > 1000000000) time = 1000000000;
			
			delta += time / 1000000000.0D;
			
			boolean render = false;
			while(delta > secondsPerTick){
				tick();
				delta -= secondsPerTick;
				render = true;
				
				ticks++;
				if(ticks % 60 == 0){
					System.out.println(frames + " fps");
					lastTime += 1000;
					frames = 0;
				}
			}
			
			if(render){
				render();
				frames++;
			}else{
				try{
					Thread.sleep(1);
				}catch(InterruptedException e){
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Game logic
	 * Updates 60 times per second
	 */
	private void tick(){
		if(input.windowFocused){
			game.tick();
			Text.updateText();
		}
	}
	
	/**
	 * Display stuff to the screen
	 * Updates 60 times per second
	 */
	private void render(){
		BufferStrategy bs = this.getBufferStrategy();
		if(bs == null){
			this.createBufferStrategy(2);
			return;
		}
		
		game.render();
		
		for(int i = 0; i < pixels.length; i++){
			pixels[i] = screen.pixels[i];
		}
		
		Graphics g = bs.getDrawGraphics();
		g.fillRect(0, 0, WIDTH * SCALE, HEIGHT * SCALE);
		g.drawImage(image, 0, 0, WIDTH * SCALE, HEIGHT * SCALE, null);
		Text.render(g, SCALE);
		g.dispose();
		bs.show();
	}
	
	/**
	 * Gets the container's input source
	 * @return The input
	 */
	public Input getInput(){
		return input;
	}
}