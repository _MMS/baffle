package com.mms.maze.gui;

import java.util.ArrayList;
import java.util.List;

import com.mms.maze.Game;
import com.mms.maze.graphics.Screen;
import com.mms.maze.gui.utils.GUIElement;

public abstract class Menu {

	protected Game game;
	protected List<GUIElement> elements;
	
	public Menu(Game game){
		this.game = game;
		elements = new ArrayList<GUIElement>();
		createElements();
	}
	
	/**
	 * Create the elements of the menu
	 */
	protected abstract void createElements();
	
	/**
	 * Add an element to the menu
	 * @param element
	 */
	public void addElement(GUIElement element){
		elements.add(element);
	}
	
	/**
	 * Gets the game the menu is tied to
	 * @return
	 */
	public Game getGame(){
		return game;
	}
	
	/**
	 * Update the menu elements' logic
	 */
	public void tick(){
		for(GUIElement e : elements){
			e.tick();
		}
	}
	
	/**
	 * Draw the menu elements
	 * @param screen
	 */
	public void render(Screen screen){
		for(GUIElement e : elements){
			e.render(screen);
		}
	}
}