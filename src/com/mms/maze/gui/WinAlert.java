package com.mms.maze.gui;

import com.mms.maze.Game;
import com.mms.maze.Game.GameState;
import com.mms.maze.graphics.Bitmap;
import com.mms.maze.gui.utils.Background;
import com.mms.maze.gui.utils.Box;
import com.mms.maze.gui.utils.Button;
import com.mms.maze.gui.utils.Button.ButtonSize;
import com.mms.maze.gui.utils.Label;

public class WinAlert extends Menu{
	
	private Label time;
	
	public WinAlert(Game game) {
		super(game);
	}
	
	public void updateTime(){
		int secs = game.time / 60;
		int mins = secs / 60;
		if(mins > 0) secs -= (mins * 60);
		
		time.setText("You won in " + mins + " minutes and " + secs + " seconds!");
	}
	
	@SuppressWarnings("unused")
	@Override
	protected void createElements() {
		Background background = new Background((Menu) this, new Bitmap(1, 1));
		background.setImage(game.lastImage);
		
		Box outline = new Box((Menu) this, 25, 25, game.getWidth() - 50, game.getHeight() - 50, 0x888888);
		Box side1 = new Box((Menu) this, 25, 25, game.getWidth() - 50, 1, 0);
		Box side2 = new Box((Menu) this, 25, 25, 1, game.getHeight() - 50, 0);
		Box side3 = new Box((Menu) this, 25, game.getHeight() - 25, game.getWidth() - 50, 1, 0);
		Box side4 = new Box((Menu) this, game.getWidth() - 25, 25, 1, game.getHeight() - 50, 0);
		
		Label win = new Label((Menu) this, "You Won!", 53, 60, 0xF4F4F4, 30);
		Button cont = new Button((Menu) this, "Menu", 90, 75, ButtonSize.LARGE, game.getInput());
		cont.setAction(cont.new ButtonAction(){
			@Override
			public void onAction(){
				game.setState(GameState.IN_MENU);
			}
		});
		time = new Label((Menu) this, "You finished in 0 seconds!", 35, 120, 0xf4f4f4, 10);
	}

}