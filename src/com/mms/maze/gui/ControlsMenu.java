package com.mms.maze.gui;

import com.mms.maze.Game;
import com.mms.maze.graphics.Bitmap;
import com.mms.maze.gui.utils.Background;
import com.mms.maze.gui.utils.Box;
import com.mms.maze.gui.utils.Button;
import com.mms.maze.gui.utils.Button.ButtonSize;
import com.mms.maze.gui.utils.Label;

public class ControlsMenu extends Menu{

	public ControlsMenu(Game game) {
		super(game);
	}

	@SuppressWarnings("unused")
	@Override
	protected void createElements() {
		Background background = new Background((Menu) this, new Bitmap("/tex/gui/title.png"));
		Label title = new Label((Menu) this, "Controls", 75, 30, 0xE3E3E3, 24);
		Box underline = new Box((Menu) this, 80, 33, 83, 1, 0x111111);
		Button back = new Button((Menu) this, "Back", 5, 5, ButtonSize.LARGE, game.getInput());
		back.setAction(back.new ButtonAction(){
			@Override
			public void onAction(){
				game.setCurrentMenu(game.mainMenu);
			}
		});
		
		Label aboutLabel1 = new Label((Menu) this, "W: Forward", 60, 50, 0xffffff, 14);
		Label aboutLabel2 = new Label((Menu) this, "A: Left", 60, 70, 0xffffff, 14);
		Label aboutLabel3 = new Label((Menu) this, "S: Backward", 60, 90, 0xffffff, 14);
		Label aboutLabel4 = new Label((Menu) this, "D: Right", 60, 110, 0xffffff, 14);
		Label aboutLabel5 = new Label((Menu) this, "ESC: Pause", 60, 130, 0xffffff, 14);
	}
	
}