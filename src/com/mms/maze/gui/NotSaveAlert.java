package com.mms.maze.gui;

import com.mms.maze.Game;
import com.mms.maze.Game.GameState;
import com.mms.maze.gui.utils.Box;
import com.mms.maze.gui.utils.Button;
import com.mms.maze.gui.utils.Label;
import com.mms.maze.gui.utils.Button.ButtonSize;

public class NotSaveAlert extends Menu{
	
	private PauseMenu owner;
	
	public NotSaveAlert(Game game, PauseMenu owner) {
		super(game);
		this.owner = owner;
	}
	
	public void setupButton(){
		Button save = new Button((Menu) this, "Save", 61, 110, ButtonSize.LARGE, game.getInput());
		save.setAction(save.new ButtonAction(){
			@Override
			public void onAction(){
				game.getSave().saveGame();
				owner.renderAlert = false;
			}
		});
		Button cont = new Button((Menu) this, "Exit", 128, 110, ButtonSize.LARGE, game.getInput());
		cont.setAction(cont.new ButtonAction(){
			@Override
			public void onAction(){
				game.setState(GameState.IN_MENU);
			}
		});
	}

	@SuppressWarnings("unused")
	@Override
	protected void createElements() {
		Box outline = new Box((Menu) this, 49, 50, 150, 105, 0x888888);
		Box side1 = new Box((Menu) this, 49, 50, 150, 1, 0);
		Box side2 = new Box((Menu) this, 49, 50, 1, 105, 0);
		Box side3 = new Box((Menu) this, 49, 155, 150, 1, 0);
		Box side4 = new Box((Menu) this, 199, 50, 1, 105, 0);
		Label label = new Label((Menu) this, "You have not saved your", 53, 65, 0xffffff, 12);
		Label label2 = new Label((Menu) this, "game! Save before exiting.", 53, 80, 0xffffff, 12);
	}

}
