package com.mms.maze.gui;

import java.io.File;

import com.mms.maze.Game;
import com.mms.maze.Game.GameState;
import com.mms.maze.graphics.Bitmap;
import com.mms.maze.gui.utils.Background;
import com.mms.maze.gui.utils.Box;
import com.mms.maze.gui.utils.Button;
import com.mms.maze.gui.utils.Button.ButtonSize;
import com.mms.maze.gui.utils.Label;

public class LoadMenu extends Menu{

	public LoadMenu(Game game) {
		super(game);
	}

	@Override
	protected void createElements() {
		refreshElements();
	}

	@SuppressWarnings("unused")	
	public void refreshElements(){
		elements.clear();
		
		Background background = new Background((Menu) this, new Bitmap("/tex/gui/title.png"));
		Label title = new Label((Menu) this, "Load Save", 65, 30, 0xE3E3E3, 24);
		Box underline = new Box((Menu) this, 80, 33, 83, 1, 0x111111);
		Button back = new Button((Menu) this, "Back", 2, 7, ButtonSize.LARGE, game.getInput());
		back.setAction(back.new ButtonAction(){
			@Override
			public void onAction(){
				game.setCurrentMenu(game.mainMenu);
			}
		});
		Box save1 = new Box((Menu) this, 5, 40, game.getWidth() - 10, 40, 0xAAAAAA);
		Box save2 = new Box((Menu) this, 5, 87, game.getWidth() - 10, 40, 0xAAAAAA);
		Box save3 = new Box((Menu) this, 5, 135, game.getWidth() - 10, 40, 0xAAAAAA);
		
		game.getSave().refreshSaves();
		if(game.getSave().saves[0]){
			Label save1Label = new Label((Menu) this, "Save 1", 10, 65, 0xffffff, 18);
			Button save1Load = new Button((Menu) this, "Load", 100, 50, ButtonSize.LARGE, game.getInput());
			save1Load.setAction(save1Load.new ButtonAction(){
				@Override
				public void onAction(){
					game.saveNum = 1;
					game.getSave().loadSave();
					game.setState(GameState.PLAYING);
				}
			});
			Button save1Del = new Button((Menu) this, "Delete", 170, 50, ButtonSize.LARGE, game.getInput());
			save1Del.setAction(save1Del.new ButtonAction(){
				@Override
				public void onAction(){
					File f = new File("saves/1.sav");
					f.delete();
					game.getSave().refreshSaves();
					game.setCurrentMenu(game.mainMenu);
				}
			});
			
		}else{
			Label save1NoLabel = new Label((Menu) this, "No Save", 82, 65, 0xffffff, 18);
			save1.setColor(0x666666);
		}
		if(game.getSave().saves[1]){
			Label save2Label = new Label((Menu) this, "Save 2", 10, 112, 0xffffff, 18);
			Button save2Load = new Button((Menu) this, "Load", 100, 97, ButtonSize.LARGE, game.getInput());
			save2Load.setAction(save2Load.new ButtonAction(){
				@Override
				public void onAction(){
					game.saveNum = 2;
					game.getSave().loadSave();
					game.setState(GameState.PLAYING);
				}
			});
			Button save2Del = new Button((Menu) this, "Delete", 170, 97, ButtonSize.LARGE, game.getInput());
			save2Del.setAction(save2Del.new ButtonAction(){
				@Override
				public void onAction(){
					File f = new File("saves/2.sav");
					f.delete();
					game.getSave().refreshSaves();
					game.setCurrentMenu(game.mainMenu);
				}
			});
		}else{
			Label save2NoLabel = new Label((Menu) this, "No Save", 82, 112, 0xffffff, 18);
			save2.setColor(0x666666);
		}
		if(game.getSave().saves[2]){
			Label save3Label = new Label((Menu) this, "Save 3", 10, 160, 0xffffff, 18);
			Button save3Load = new Button((Menu) this, "Load", 100, 145, ButtonSize.LARGE, game.getInput());
			save3Load.setAction(save3Load.new ButtonAction(){
				@Override
				public void onAction(){
					game.saveNum = 3;
					game.getSave().loadSave();
					game.setState(GameState.PLAYING);
				}
			});
			Button save3Del = new Button((Menu) this, "Delete", 170, 145, ButtonSize.LARGE, game.getInput());
			save3Del.setAction(save3Del.new ButtonAction(){
				@Override
				public void onAction(){
					File f = new File("saves/3.sav");
					f.delete();
					game.getSave().refreshSaves();
					game.setCurrentMenu(game.mainMenu);
				}
			});
		}else{
			Label save3NoLabel = new Label((Menu) this, "No Save", 82, 160, 0xffffff, 18);
			save3.setColor(0x666666);
		}
	}
}