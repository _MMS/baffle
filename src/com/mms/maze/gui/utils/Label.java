package com.mms.maze.gui.utils;

import com.mms.maze.graphics.Screen;
import com.mms.maze.graphics.Text;
import com.mms.maze.gui.Menu;

public class Label extends GUIElement{

	private Text text;
	
	public Label(Menu owner, String text, int x, int y, int color, int size) {
		super(owner, "label");
		this.text = new Text(text, x, y, color, size);
	}
	
	/**
	 * Sets the text of the label
	 * @param text
	 */
	public void setText(String text){
		this.text.setText(text);
	}
	
	/**
	 * Set whether or not the label is centered
	 * @param centered
	 */
	public void centered(boolean centered){
		text.centered(centered);
	}

	@Override
	public void tick() {
	}

	@Override
	public void render(Screen screen) {
		Text.addToTextQueue(text);
	}

}