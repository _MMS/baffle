package com.mms.maze.gui.utils;

import com.mms.maze.graphics.Bitmap;
import com.mms.maze.graphics.Screen;
import com.mms.maze.gui.Menu;

public class Background extends GUIElement{
	
	private Bitmap image;
	
	private boolean scrolling = false;
	private int scrollRate = 1;
	private int scroll = 0;
	
	private int x, y;
	
	/**
	 * Instantiates a new background
	 * @param owner
	 * @param path
	 */
	public Background(Menu owner, Bitmap path){
		this(owner, path, 0, 0);
	}
	
	public Background(Menu owner, Bitmap path, int x, int y){
		super(owner, "background");
		image = path;
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Whether or not the background is scrolling
	 * @param scrolling
	 */
	public void setScrolling(boolean scrolling){
		this.scrolling = scrolling;
	}
	
	/**
	 * Whether or not the background is scrolling
	 * @param scrolling
	 */
	public boolean isScrolling(){
		return scrolling;
	}
	
	/**
	 * Sets the scroll speed of the background
	 * @param speed
	 */
	public void setScrollSpeed(int speed){
		this.scrollRate = speed;
	}
	
	/**
	 * Set the image of the background
	 * @param image
	 */
	public void setImage(Bitmap image){
		this.image = image;
	}
	
	@Override
	public void tick(){
		if(scrolling){
			if(scroll < image.getWidth() - owner.getGame().getWidth()){
				scroll += scrollRate;
			}else{
				scroll = 0;
			}
		}
	}
	
	@Override
	public void render(Screen screen){
		screen.drawMap(image, x - (scrolling ? scroll : 0), y, false);
	}
}