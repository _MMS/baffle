package com.mms.maze.gui.utils;

import com.mms.maze.graphics.Bitmap;
import com.mms.maze.graphics.Screen;
import com.mms.maze.gui.Menu;

public class Box extends GUIElement{
	
	private int x, y;
	private Bitmap image;
	
	/**
	 * Creates a new box
	 * @param owner Owner of the box
	 * @param x X position of the box
	 * @param y Y position of the box
	 * @param width Width of the box
	 * @param height Height of the box
	 * @param color Color of the box
	 */
	public Box(Menu owner, int x, int y, int width, int height, int color){
		super(owner, "box");
		this.x = x;
		this.y = y;
		this.image = new Bitmap(width, height, color);
	}
	
	/**
	 * Set the color of the box
	 * @param color
	 */
	public void setColor(int color){
		image = new Bitmap(image.getWidth(), image.getHeight(), color);
	}

	@Override
	public void tick() {
	}

	@Override
	public void render(Screen screen) {
		screen.drawMap(image, x, y, false);
	}

}