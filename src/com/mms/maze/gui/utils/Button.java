package com.mms.maze.gui.utils;

import com.mms.maze.Controls;
import com.mms.maze.Input;
import com.mms.maze.graphics.Bitmap;
import com.mms.maze.graphics.Screen;
import com.mms.maze.graphics.Text;
import com.mms.maze.gui.Menu;

public class Button extends GUIElement{
	
	private Bitmap corner_normal = new Bitmap("/tex/gui/button.png", 0, 0, 3);
	private Bitmap horizontal_side_normal = new Bitmap("/tex/gui/button.png", 1, 0, 3);
	private Bitmap vertical_side_normal = new Bitmap("/tex/gui/button.png", 0, 1, 3);
	private Bitmap filler_normal = new Bitmap("/tex/gui/button.png", 1, 1, 3);
	private Bitmap corner_hover = new Bitmap("/tex/gui/button.png", 0, 0, 3);
	private Bitmap horizontal_side_hover = new Bitmap("/tex/gui/button.png", 1, 0, 3);
	private Bitmap vertical_side_hover = new Bitmap("/tex/gui/button.png", 0, 1, 3);
	private Bitmap filler_hover = new Bitmap("/tex/gui/button.png", 1, 1, 3);
	private Bitmap corner_click = new Bitmap("/tex/gui/button.png", 0, 0, 3);
	private Bitmap horizontal_side_click = new Bitmap("/tex/gui/button.png", 1, 0, 3);
	private Bitmap vertical_side_click = new Bitmap("/tex/gui/button.png", 0, 1, 3);
	private Bitmap filler_click = new Bitmap("/tex/gui/button.png", 1, 1, 3);
	
	private String text;
	private int x, y, width, height;
	private ButtonSize size;
	private Input input;
	private ButtonAction action;
	private static long lastAction = System.currentTimeMillis();
	
	private Bitmap corner;
	private Bitmap horizontal_side;
	private Bitmap vertical_side;
	private Bitmap filler;
	
	/**
	 * New button
	 * @param owner Owner of the button
	 * @param text Text of the button
	 * @param x X position of the button
	 * @param y Y position of the button
	 * @param size Size of the button
	 * @param input Input the button uses
	 */
	public Button(Menu owner, String text, int x, int y, ButtonSize size, Input input){
		super(owner, "button");
		this.text = text;
		this.x = x;
		this.y = y;
		this.size = size;
		size();
		this.input = input;
		setupImages();
	}
	
	/**
	 * Color button images
	 */
	private void setupImages(){
		corner_normal.setColors(0x000000, 0x555554, 0xff00ff, 0xDDDDDD);
		horizontal_side_normal.setColors(0x000000, 0x555554, 0xff00ff, 0xDDDDDD);
		vertical_side_normal.setColors(0x000000, 0x555554, 0xff00ff, 0xDDDDDD);
		filler_normal.setColors(0x000000, 0x555554, 0xff00ff, 0xDDDDDD);
		corner_hover.setColors(0x000000, 0x777777, 0xff00ff, 0xFFFFFF);
		horizontal_side_hover.setColors(0x000000, 0x777777, 0xff00ff, 0xFFFFFF);
		vertical_side_hover.setColors(0x000000, 0x777777, 0xff00ff, 0xFFFFFF);
		filler_hover.setColors(0x000000, 0x777777, 0xff00ff, 0xFFFFFF);
		corner_click.setColors(0x000000, 0x444444, 0xff00ff, 0x999999);
		horizontal_side_click.setColors(0x000000, 0x444444, 0xffff00ff, 0x999999);
		vertical_side_click.setColors(0x000000, 0x444444, 0xff00ff, 0x999999);
		filler_click.setColors(0x000000, 0x444444, 0xff00ff, 0x999999);
		corner = corner_normal;
		horizontal_side = horizontal_side_normal;
		vertical_side = vertical_side_normal;
		filler = filler_normal;
	}
	
	/**
	 * Sets action the button performs when clicked
	 * @param action
	 */
	public void setAction(ButtonAction action){
		this.action = action;
	}
	
	/**
	 * Size the button correctly
	 */
	private void size(){
		switch(size){
		case SMALL:
			width = 30;
			height = 15;
			break;
		case MEDIUM:
			width = 48;
			height = 18;
			break;
		case LARGE:
			width = 60;
			height = 21;
			break;
		default:
			width = 0;
			height = 0;
		}
	}
	
	@Override
	public void tick(){
		if(input.mouseX > x && input.mouseX < x + width - 1 && input.mouseY > y && input.mouseY < y + height - 1){
			if(input.mouseClick == Controls.LEFT_CLICK){
				if(action != null && System.currentTimeMillis() - lastAction > 200){
					action.onAction();
					lastAction = System. currentTimeMillis();
				}
				corner = corner_click;
				horizontal_side = horizontal_side_click;
				vertical_side = vertical_side_click;
				filler = filler_click;
			}else{
				corner = corner_hover;
				horizontal_side = horizontal_side_hover;
				vertical_side = vertical_side_hover;
				filler = filler_hover;
			}
		}else{
			corner = corner_normal;
			horizontal_side = horizontal_side_normal;
			vertical_side = vertical_side_normal;
			filler = filler_normal;
		}
	}
	
	@Override
	public void render(Screen screen){
		if(width < 9 || height < 9) return;
		
		for(int y = 0; y < Math.ceil(height / 3); y++){
			for(int x = 0; x < Math.ceil(width / 3); x++){
				if(x == 0 && y == 0){
					screen.drawMap(corner, this.x, this.y, false);
					continue;
				}else if(x == 0 && y == Math.ceil(height / 3) - 1){
					screen.drawMap(corner, this.x, this.y + height - 4, Screen.BIT_FLIP_Y, false);
					continue;
				}else if(y == 0 && x == Math.ceil(width / 3) - 1){
					screen.drawMap(corner, this.x + width - 4, this.y, Screen.BIT_FLIP_X, false);
					continue;
				}else if(y == Math.ceil(height / 3) - 1 && x == Math.ceil(width / 3) - 1){
					screen.drawMap(corner, this.x + width - 4, this.y + height - 4, Screen.BIT_FLIP_BOTH, false);
					continue;
				}else if(y == 0){ 
					screen.drawMap(horizontal_side, this.x + (x * 3), this.y, false);
					continue;
				}else if(y == Math.ceil(height / 3) - 1){ 
					screen.drawMap(horizontal_side, this.x + (x * 3), this.y + height - 4, Screen.BIT_FLIP_Y, false);
					continue;
				}else if(x == 0){ 
					screen.drawMap(vertical_side, this.x, this.y + (y * 3), false);
					continue;
				}else if(x == Math.ceil(width / 3) - 1){ 
					screen.drawMap(vertical_side, this.x + width - 4, this.y + (y * 3), Screen.BIT_FLIP_X, false);
					continue;
				}else{
					screen.drawMap(filler, this.x + (x * 3), this.y + (y * 3), false);
				}
			}
		}
		
		int textSize = height - 8;
		int yOffset = 4;
		int xOffset = 0;
		switch(size){
		case MEDIUM:
			xOffset = 1;
			break;
		case LARGE:
			xOffset = 2;
			break;
		default:
			break;
		}
		Text rendText = new Text(text, x + width / 2 + xOffset, y + yOffset, 0x000000, textSize);
		rendText.centered(true);
		Text.addToTextQueue(rendText);
	}
	
	public enum ButtonSize{
		SMALL,
		MEDIUM,
		LARGE
	}
	
	public class ButtonAction{
		public void onAction(){
		}
	}
}