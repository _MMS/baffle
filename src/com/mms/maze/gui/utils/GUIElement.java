package com.mms.maze.gui.utils;

import com.mms.maze.graphics.Screen;
import com.mms.maze.gui.Menu;

public abstract class GUIElement {
	
	protected Menu owner;
	protected String name;
	
	public GUIElement(Menu owner, String name){
		this.owner = owner;
		this.name = name;
		owner.addElement(this);
	}
	
	/**
	 * Update the element's logic
	 */
	public abstract void tick();
	/**
	 * Draw the element
	 * @param screen
	 */
	public abstract void render(Screen screen);

}