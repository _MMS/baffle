package com.mms.maze.gui;

import com.mms.maze.Game;
import com.mms.maze.Game.GameState;
import com.mms.maze.graphics.Bitmap;
import com.mms.maze.graphics.Screen;
import com.mms.maze.gui.utils.Background;
import com.mms.maze.gui.utils.Box;
import com.mms.maze.gui.utils.Button;
import com.mms.maze.gui.utils.GUIElement;
import com.mms.maze.gui.utils.Button.ButtonSize;
import com.mms.maze.gui.utils.Label;

public class PauseMenu extends Menu{
	
	private Background background;
	public boolean renderAlert = false;
	private NotSaveAlert notSaveAlert;
	
	public PauseMenu(Game game) {
		super(game);
		notSaveAlert = new NotSaveAlert(game, this);
		notSaveAlert.setupButton();
	}
	
	public void updateBackground(){
		background.setImage(game.lastImage);
	}
	
	@SuppressWarnings("unused")
	@Override
	protected void createElements() {
		background = new Background((Menu) this, new Bitmap(1, 1));
		Box outline = new Box((Menu) this, 50, 5, game.getWidth() - 100, game.getHeight() - 10, 0x888888);
		Box side1 = new Box((Menu) this, 50, 5, game.getWidth() - 100, 1, 0);
		Box side2 = new Box((Menu) this, 50, 5, 1, game.getHeight() - 10, 0);
		Box side3 = new Box((Menu) this, 50, game.getHeight() - 5, game.getWidth() - 100, 1, 0);
		Box side4 = new Box((Menu) this, game.getWidth() - 50, 5, 1, game.getHeight() - 10, 0);
		
		Label title = new Label((Menu) this, "Paused", 80, 28, 0xF4F4F4, 24);
		Box underline = new Box((Menu) this, 75, 31, 91, 1, 0);
		
		Button back = new Button((Menu) this, "Return", 90, 40, ButtonSize.LARGE, game.getInput());	
		back.setAction(back.new ButtonAction(){
			@Override
			public void onAction(){
				game.setState(GameState.PLAYING);
			}
		});
		Button save = new Button((Menu) this, "Save", 90, 65, ButtonSize.LARGE, game.getInput());
		save.setAction(save.new ButtonAction(){
			@Override
			public void onAction(){
				game.getSave().saveGame();
			}
		});
		Button quit = new Button((Menu) this, "Exit ", 90, 90, ButtonSize.LARGE, game.getInput());
		quit.setAction(quit.new ButtonAction(){
			@Override
			public void onAction(){
				game.getSave().exit(game.pauseMenu);
			}
		});
	}
	
	@Override
	public void tick(){
		for(GUIElement e : elements){
			if(renderAlert && e instanceof Button) continue;
			e.tick();
		}
		if(renderAlert) notSaveAlert.tick();
	}
	
	@Override
	public void render(Screen screen){
		for(GUIElement e : elements){
			if(renderAlert && e instanceof Button) continue;
			e.render(screen);
		}
		if(renderAlert) notSaveAlert.render(screen);
	}
}