package com.mms.maze.gui;

import com.mms.maze.Game;
import com.mms.maze.Game.GameState;
import com.mms.maze.graphics.Bitmap;
import com.mms.maze.graphics.Screen;
import com.mms.maze.gui.utils.Background;
import com.mms.maze.gui.utils.Box;
import com.mms.maze.gui.utils.Button;
import com.mms.maze.gui.utils.GUIElement;
import com.mms.maze.gui.utils.Button.ButtonSize;
import com.mms.maze.gui.utils.Label;

public class MainMenu extends Menu{
	
	protected boolean renderAlert = false;
	private NoRoomAlert noRoomAlert;
	
	public MainMenu(Game game) {
		super(game);
		noRoomAlert = new NoRoomAlert(game, this);
		noRoomAlert.addButton();
	}
	
	@Override
	@SuppressWarnings("unused")
	protected void createElements(){
		Background background = new Background((Menu) this, new Bitmap("/tex/gui/title.png"));
		Label title = new Label((Menu) this, "Baffle", 90, 30, 0xE3E3E3, 24);
		Box underline = new Box((Menu) this, 80, 33, 83, 1, 0x111111);
		Button newGame = new Button((Menu) this, "New", 90, 40, ButtonSize.LARGE, game.getInput());
		newGame.setAction(newGame.new ButtonAction(){
			@Override
			public void onAction(){
				game.saveNum = game.getSave().nextSaveNum();
				if(game.saveNum != -1){
					game.getSave().loadDefaultSave();
					game.getSave().saves[game.saveNum - 1] = true;
					game.setState(GameState.PLAYING);
				}else{
					renderAlert = true;
				}
			}
		});
		Button loadGame = new Button((Menu) this, "Load", 90, 65, ButtonSize.LARGE, game.getInput());
		loadGame.setAction(loadGame.new ButtonAction(){
			@Override
			public void onAction(){
				game.setCurrentMenu(game.loadMenu);
				game.loadMenu.refreshElements();
			}
		});
		Button controls = new Button((Menu) this, "Controls", 90, 90, ButtonSize.LARGE, game.getInput());
		controls.setAction(controls.new ButtonAction(){
			@Override
			public void onAction(){
				game.setCurrentMenu(game.controlsMenu);
			}
		});
		Button about = new Button((Menu) this, "About", 5, game.getHeight() - 23, ButtonSize.MEDIUM, game.getInput());
		about.setAction(about.new ButtonAction(){
			@Override
			public void onAction(){
				game.setCurrentMenu(game.aboutMenu);
			}
		});
		Button quit = new Button((Menu) this, "Quit ", game.getWidth() - 53, game.getHeight() - 23, ButtonSize.MEDIUM, game.getInput());
		quit.setAction(quit.new ButtonAction(){
			@Override
			public void onAction(){
				System.exit(-1);
			}
		});
	}
	
	@Override
	public void tick(){
		for(GUIElement e : elements){
			if(renderAlert && e instanceof Button) continue;
			e.tick();
		}
		if(renderAlert) noRoomAlert.tick();
	}
	
	@Override
	public void render(Screen screen){
		for(GUIElement e : elements){
			if(renderAlert && e instanceof Button) continue;
			e.render(screen);
		}
		if(renderAlert) noRoomAlert.render(screen);
	}

}