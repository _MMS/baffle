package com.mms.maze.gui;

import com.mms.maze.Game;
import com.mms.maze.graphics.Bitmap;
import com.mms.maze.gui.utils.Background;
import com.mms.maze.gui.utils.Box;
import com.mms.maze.gui.utils.Button;
import com.mms.maze.gui.utils.Button.ButtonSize;
import com.mms.maze.gui.utils.Label;

public class AboutMenu extends Menu{

	public AboutMenu(Game game) {
		super(game);
	}

	@Override
	@SuppressWarnings("unused")
	protected void createElements() {
		Background background = new Background((Menu) this, new Bitmap("/tex/gui/title.png"));
		Label title = new Label((Menu) this, "About", 90, 30, 0xE3E3E3, 24);
		Box underline = new Box((Menu) this, 80, 33, 83, 1, 0x111111);
		//text
		Button back = new Button((Menu) this, "Back", 5, 5, ButtonSize.LARGE, game.getInput());
		back.setAction(back.new ButtonAction(){
			@Override
			public void onAction(){
				game.setCurrentMenu(game.mainMenu);
			}
		});
		String line1 = "This game was written for";
		String line2 = "the ICS3C course by Oliver";
		String line3 = "Byl in pure Java.";
		Label aboutLabel1 = new Label((Menu) this, line1, 40, 50, 0xffffff, 14);
		Label aboutLabel2 = new Label((Menu) this, line2, 40, 70, 0xffffff, 14);
		Label aboutLabel3 = new Label((Menu) this, line3, 40, 90, 0xffffff, 14);
		Label aboutLabel4 = new Label((Menu) this, "Enjoy!", 40, 130, 0xffffff, 14);
	}

}