package com.mms.maze.gui;

import com.mms.maze.Game;
import com.mms.maze.gui.utils.Box;
import com.mms.maze.gui.utils.Button;
import com.mms.maze.gui.utils.Label;
import com.mms.maze.gui.utils.Button.ButtonSize;

public class NoRoomAlert extends Menu{
	
	private MainMenu owner;
	
	public NoRoomAlert(Game game, MainMenu owner) {
		super(game);
		this.owner = owner;
	}
	
	@SuppressWarnings("unused")
	@Override
	protected void createElements() {
		Box outline = new Box((Menu) this, 49, 50, 150, 105, 0x888888);
		Box side1 = new Box((Menu) this, 49, 50, 150, 1, 0);
		Box side2 = new Box((Menu) this, 49, 50, 1, 105, 0);
		Box side3 = new Box((Menu) this, 49, 155, 150, 1, 0);
		Box side4 = new Box((Menu) this, 199, 50, 1, 105, 0);
		Label label = new Label((Menu) this, "No more unused save files!", 53, 65, 0xffffff, 12);
		Label label2 = new Label((Menu) this, "Please select a used one.", 53, 80, 0xffffff, 12);
	}
	
	public void addButton(){
		Button button = new Button((Menu) this, "OK", 93, 100, ButtonSize.LARGE, owner.getGame().getInput());
		button.setAction(button.new ButtonAction(){
			@Override
			public void onAction(){
				owner.renderAlert = false;
			}
		});
	}

}