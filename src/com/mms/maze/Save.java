package com.mms.maze;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.StringTokenizer;

import com.mms.maze.Game.GameState;
import com.mms.maze.gui.PauseMenu;

public class Save {
	
	private Game game;
	//save state
	private int levelId = 0;
	private int playerX = 0;
	private int playerY = 0;
	private int playerDir = 0;
	private int playerProgression = 0;
	private int time = 0;
	
	public boolean[] saves = new boolean[3];
	
	public Save(Game game){
		this.game = game;
		refreshSaves();
	}
	
	/**
	 * Refresh list of what saves are full
	 */
	public void refreshSaves(){
		File save1 = new File("saves/1.sav");
		if(save1.exists()) saves[0] = true;
		else saves[0] = false;
		File save2 = new File("saves/2.sav");
		if(save2.exists()) saves[1] = true;
		else saves[1] = false;
		File save3 = new File("saves/3.sav");
		if(save3.exists()) saves[2] = true;
		else saves[2] = false;
	}
	
	/**
	 * Gets the next available save number
	 * @return 1, 2 or 3. -1 if none available
	 */
	public int nextSaveNum(){
		File save1 = new File("saves/1.sav");
		if(!save1.exists()) return 1;
		File save2 = new File("saves/2.sav");
		if(!save2.exists()) return 2;
		File save3 = new File("saves/3.sav");
		if(!save3.exists()) return 3;
		return -1;
	}
	
	/**
	 * Loads the default save file
	 */
	public void loadDefaultSave(){
		loadSave("saves/default.sav");
		levelId = -1;
	}
	
	/**
	 * Load the current game's save file
	 */
	public void loadSave(){
		loadSave("saves/" + game.saveNum + ".sav");
	}
	
	/**
	 * Load save with a specific path
	 * @param path
	 */
	private void loadSave(String path){
		try(BufferedReader reader = new BufferedReader(new FileReader(new File(path)))){
			String line;
			StringTokenizer tokenizer;
			
			line = reader.readLine();
			tokenizer = new StringTokenizer(line, " ");
			
			levelId = Integer.parseInt(tokenizer.nextToken());
			game.getLevelHandler().loadLevel(levelId);
			
			playerX = Integer.parseInt(tokenizer.nextToken());
			playerY = Integer.parseInt(tokenizer.nextToken());
			game.getPlayer().setPosition(playerX, playerY);
			
			playerDir = Integer.parseInt(tokenizer.nextToken());
			game.getPlayer().setDirection(playerDir);
			
			playerProgression = Integer.parseInt(tokenizer.nextToken());
			game.getPlayer().progression = playerProgression;
			if(levelId == 0) game.getLevelHandler().getCurrentLevel().updateProgression();
			
			time = Integer.parseInt(tokenizer.nextToken());
			game.time = time;
		}catch(FileNotFoundException e){
			System.err.println("Failed to load save: no file");
			return;
		}catch(IOException e) {
			System.err.println("Failed to load save: IO error");
			return;
		}
	}
	
	/**
	 * Saves the game
	 */
	public void saveGame(){
		try{
			BufferedWriter writer = new BufferedWriter(new FileWriter("saves/" + game.saveNum + ".sav"));
			
			levelId = game.getLevelHandler().getCurrentLevelId();
			playerX = game.getPlayer().getX();
			playerY = game.getPlayer().getY();
			playerDir = game.getPlayer().getDirection();
			playerProgression = game.getPlayer().progression;
			time = game.time;
			String data = levelId + " " + playerX + " " + playerY + " " + playerDir + " " + playerProgression + " " + time;
			
			writer.write(data);
			writer.close();
			
			refreshSaves();
		}catch(IOException e){
			System.err.println("Failed to save");
			return;
		}
	}
	
	/**
	 * Safely exit the game. Alert the player if they haven't saved the game.
	 * @param owner
	 */
	public void exit(PauseMenu owner){
		if(game.time != time){
			owner.renderAlert = true;
		}else{
			game.setState(GameState.IN_MENU);
		}
	}
}