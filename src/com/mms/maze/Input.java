package com.mms.maze;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class Input implements FocusListener, KeyListener, MouseListener, MouseMotionListener{
	
	public boolean windowFocused = true;
	public int mouseX = 0, mouseY = 0;
	public int mouseClick = -1;
	private boolean[] keys = new boolean[4096];
	
	public boolean isKeyPressed(int key){
		return keys[key];
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		keys[e.getKeyCode()] = true;
	}

	@Override
	public void keyReleased(KeyEvent e) {
		keys[e.getKeyCode()] = false;
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void focusGained(FocusEvent e) {
		windowFocused = true;
	}

	@Override
	public void focusLost(FocusEvent e) {
		windowFocused = false;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		mouseX = e.getX() / 3;
		mouseY = e.getY() / 3;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		mouseX = e.getX() / 3;
		mouseY = e.getY() / 3;
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		mouseClick = e.getButton();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		mouseClick = -1;
	}

}