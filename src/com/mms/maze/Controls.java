package com.mms.maze;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class Controls {
	//Movement controls
	public static final int FORWARD = KeyEvent.VK_W;
	public static final int BACK = KeyEvent.VK_S;
	public static final int LEFT = KeyEvent.VK_A;
	public static final int RIGHT = KeyEvent.VK_D;
	
	//Interaction controls
	public static final int PAUSE = KeyEvent.VK_ESCAPE;
	
	//Mouse controls
	public static final int LEFT_CLICK = MouseEvent.BUTTON1;
}