package com.mms.maze.graphics;

public class Sprite{
	
	//General-use tile sprites
	public static final Bitmap level_incomplete_sprite = new Bitmap("/tex/tiles.png", 5, 0, 16);
	public static final Bitmap level_complete_sprite = new Bitmap("/tex/tiles.png", 6, 0, 16);
	public static final Bitmap level_next_sprite = new Bitmap("/tex/tiles.png", 7, 0, 16);
	public static final Bitmap level_door_sprite = new Bitmap("/tex/tiles.png", 6, 1, 16);
	public static final Bitmap level_wall_sprite = new Bitmap("/tex/tiles.png", 7, 1, 16);
	
	//Dungeon tile sprites
	public static final Bitmap dungeon_wall_sprite = new Bitmap("/tex/tiles.png", 0, 0, 16);
	public static final Bitmap dungeon_window_sprite = new Bitmap("/tex/tiles.png", 1, 0, 16);
	public static final Bitmap dungeon_torch_sprite = new Bitmap("/tex/tiles.png", 2, 0, 16);
	public static final Bitmap dungeon_wall_cracked_sprite = new Bitmap("/tex/tiles.png", 3, 0, 16);
	public static final Bitmap dungeon_floor_sprite = new Bitmap("/tex/tiles.png", 0, 1, 16);
	
	//Cave tile sprites
	public static final Bitmap cave_wall_1_sprite = new Bitmap("/tex/tiles.png", 0, 2, 16);
	public static final Bitmap cave_wall_2_sprite = new Bitmap("/tex/tiles.png", 1, 2, 16);
	public static final Bitmap cave_wall_3_sprite = new Bitmap("/tex/tiles.png", 2, 2, 16);
	public static final Bitmap cave_water_sprite = new Bitmap("/tex/tiles.png", 0, 3, 16);
	public static final Bitmap cave_bridge_vertical_sprite = new Bitmap("/tex/tiles.png", 1, 3, 16);
	public static final Bitmap cave_bridge_horizontal_sprite = new Bitmap("/tex/tiles.png", 2, 3, 16);
	public static final Bitmap cave_path_vertical_sprite = new Bitmap("/tex/tiles.png", 4, 2, 16);
	public static final Bitmap cave_path_horizontal_sprite = new Bitmap("/tex/tiles.png", 5, 2, 16);
	public static final Bitmap cave_path_corner_1_sprite = new Bitmap("/tex/tiles.png", 6, 2, 16);
	public static final Bitmap cave_path_corner_2_sprite = new Bitmap("/tex/tiles.png", 7, 2, 16);
	public static final Bitmap cave_path_corner_3_sprite = new Bitmap("/tex/tiles.png", 6, 3, 16);
	public static final Bitmap cave_path_corner_4_sprite = new Bitmap("/tex/tiles.png", 7, 3, 16);
	
	//Forest tile sprites
	public static final Bitmap forest_wall_sprite = new Bitmap("/tex/tiles.png", 0, 4, 16);
	public static final Bitmap forest_floor_sprite = new Bitmap("/tex/tiles.png", 1, 4, 16);
	
	//Player sprites
	public static final Bitmap player_forward_1_sprite = new Bitmap("/tex/ent/player.png", 0, 0, 16);
	public static final Bitmap player_side_1_sprite = new Bitmap("/tex/ent/player.png", 1, 0, 16);
	public static final Bitmap player_back_1_sprite = new Bitmap("/tex/ent/player.png", 2, 0, 16);
	public static final Bitmap player_forward_2_sprite = new Bitmap("/tex/ent/player.png", 0, 1, 16);
	public static final Bitmap player_side_2_sprite = new Bitmap("/tex/ent/player.png", 1, 1, 16);
	public static final Bitmap player_back_2_sprite = new Bitmap("/tex/ent/player.png", 2, 1, 16);
	public static final Bitmap player_forward_3_sprite = new Bitmap("/tex/ent/player.png", 0, 2, 16);
	public static final Bitmap player_side_3_sprite = new Bitmap("/tex/ent/player.png", 1, 2, 16);
	public static final Bitmap player_back_3_sprite = new Bitmap("/tex/ent/player.png", 2, 2, 16);
	
	/**
	 * Add color to all the sprites
	 */
	public static void colorSprites(){
		level_incomplete_sprite.setColors(0x000000, 0x555555, 0xff0000, 0xffffff);
		level_complete_sprite.setColors(0x000000, 0x555555, 0x00ff00, 0xffffff);
		level_next_sprite.setColors(0x0, 0xAAAA88, 0xEEEEAA, 0x0);
		
		cave_wall_1_sprite.setColors(0x774444, 0x885555, 0x996666, 0xBB8888);
		cave_path_vertical_sprite.setColors(0x774444, 0x885555, 0xEEAAAA, 0xBB8888);
		cave_path_horizontal_sprite.setColors(0x774444, 0x885555, 0xEEAAAA, 0xBB8888);
		cave_path_corner_1_sprite.setColors(0x774444, 0x885555, 0xEEAAAA, 0xBB8888);
		cave_path_corner_2_sprite.setColors(0x774444, 0x885555, 0xEEAAAA, 0xBB8888);
		cave_path_corner_3_sprite.setColors(0x774444, 0x885555, 0xEEAAAA, 0xBB8888);
		cave_path_corner_4_sprite.setColors(0x774444, 0x885555, 0xEEAAAA, 0xBB8888);
		
		forest_wall_sprite.setColors(0x144400, 0x195100, 0x1D6300, 0x278200);
		forest_floor_sprite.setColors(0x144400, 0x195100, 0x1D6000, 0x278200);
		
		dungeon_wall_sprite.setColors(0x000000, 0x333377, 0x7777AA, 0xAAAAEE);
		dungeon_floor_sprite.setColors(0x000000, 0x667766, 0x889988, 0xDDDDFF);
	}
	
}