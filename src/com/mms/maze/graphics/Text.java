package com.mms.maze.graphics;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

public class Text {

	private static List<Text> textQueue = new ArrayList<Text>();
	private static List<Text> textRender = new ArrayList<Text>();
	private static List<Text> renderedLastFrame = new ArrayList<Text>();
	
	private String text;
	private int x, y;
	private int color;
	private int size;
	private boolean centered = false;
	
	/**
	 * Create new text
	 * @param text Text to display
	 * @param x Location on screen
	 * @param y Location on screen
	 * @param color Color of the text
	 * @param size Size of the text
	 */
	public Text(String text, int x, int y, int color, int size){
		this.text = text;
		this.x = x;
		this.y = y;
		this.color = color;
		this.size = size;
	}
	
	/**
	 * Sets text
	 * @param text
	 */
	public void setText(String text){
		this.text = text;
	}
	
	/**
	 * Whether or not the text is centered around its x,y point
	 * @param centered
	 */
	public void centered(boolean centered){
		this.centered = centered;
	}
	
	/**
	 * Add a piece of text to be rendered
	 * @param textToAdd
	 */
	public static void addToTextQueue(Text textToAdd){
		textQueue.add(textToAdd);
	}
	
	/**
	 * Update the text rendering queue
	 */
	public static void updateText(){
		for(Text t : textQueue){
			if(!textRender.contains(t)) textRender.add(t);
		}
		for(Text t : renderedLastFrame){
			if(!textQueue.contains(t)) textRender.remove(t);
		}
		textQueue.clear();
	}
	
	/**
	 * Render the text in the render queue
	 * @param g Graphics to render with
	 * @param scale Scale of the screen
	 */
	public static void render(Graphics g, int scale){
		try{
			for(Text t : textRender){
				g.setColor(new Color(t.color));
				g.setFont(new Font("Arial", Font.PLAIN, t.size * scale));
				int x = 0;
				int y = 0;
				if(t.centered){
					FontMetrics metrics = g.getFontMetrics();
					x = t.x - (metrics.stringWidth(t.text) / 4 - t.text.length());
					y = t.y + metrics.getHeight() / 4;
				}else{
					x = t.x;
					y = t.y;
				}
				g.drawString(t.text, x * scale, y * scale);
				renderedLastFrame.add(t);
			}
		}catch(ConcurrentModificationException e){
			return;
		}
	}
}