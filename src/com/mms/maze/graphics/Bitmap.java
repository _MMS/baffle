package com.mms.maze.graphics;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Bitmap {

	//holds values for the bitmap
	public int[] pixels;
	protected int width, height;
	protected int scale = 1;
	protected int transparentColor = 0xffff00ff;
	protected int fillColor = 0x000000;
	
	/**
	 * Instantiates the bitmap with a solid color
	 * @param width Width of the bitmap.
	 * @param height Height of the bitmap.
	 */
	public Bitmap(int width, int height, int color){
		this.width = width;
		this.height = height;
		this.pixels = new int[width * height];
		for(int i = 0; i < pixels.length; i++){
			pixels[i] = color;
		}
	}
	
	/**
	 * Instantiates the bitmap as a blank map
	 * @param width Width of the bitmap.
	 * @param height Height of the bitmap.
	 */
	public Bitmap(int width, int height){
		this(width, height, 0x000000);
	}
	
	/**
	 * Instantiates the bitmap from an image file.
	 * @param path Path of the image file for the bitmap to emulate.
	 */
	public Bitmap(String path){
		BufferedImage image;
		try{
			image = ImageIO.read(Bitmap.class.getResource(path));
			
			width = image.getWidth();
			height = image.getHeight();
			pixels = image.getRGB(0, 0, width, height, null, 0, width);
		}catch(IOException e){
			System.err.println("Failed to load bitmap from file " + path);
		}
	}
	
	/**
	 * Instantiates the bitmap as a cropped section from an image.
	 * @param path Path of the image file for the bitmap to emulate.
	 * @param sheetX The x value in the image's grid of bitmaps.
	 * @param sheetY The y value in the image's grid of bitmaps.
	 * @param size The size of the bitmaps in the image's grid.
	 */
	public Bitmap(String path, int sheetX, int sheetY, int size){
		this(path);
		int[] crop = new int[size * size];
		for(int y = 0; y < size; y++){
			for(int x = 0; x < size; x++){
				crop[x + y * size] = pixels[((sheetX * size) + x) + ((sheetY * size) + y) * width];
			}
		}
		width = size;
		height = size;
		pixels = crop;
	}
	
	/**
	 * Gets the width of the bitmap.
	 * @return The bitmaps's width.
	 */
	public int getWidth(){
		return width;
	}
	
	/**
	 * Gets the height of the bitmap.
	 * @return The bitmap's height.
	 */
	public int getHeight(){
		return height;
	}
	
	/**
	 * Gets the scale of the bitmap
	 * @return The bitmap's scale
	 */
	public int getScale(){
		return scale;
	}
	
	/**
	 * Sets the scale of the bitmap
	 * @param scale
	 */
	public void setScale(int scale){
		this.scale = scale;
	}
	
	/**
	 * Sets the colors of the bitmap.
	 * @param col1 The first color.
	 * @param col2 The second color.
	 * @param col3 The third color.
	 * @param col4 The fourth color.
	 */
	public void setColors(int col1, int col2, int col3, int col4){
		for(int i = 0; i < pixels.length; i++){
			switch(pixels[i]){
			case 0xff000000:
				pixels[i] = col1;
				break;
			case 0xff555555:
				pixels[i] = col2;
				break;
			case 0xffAAAAAA:
				pixels[i] = col3;
				break;
			case 0xffffffff:
				pixels[i] = col4;
				break;
			default:
			}
		}
	}
	
}