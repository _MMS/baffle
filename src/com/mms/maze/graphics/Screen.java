package com.mms.maze.graphics;

public class Screen extends Bitmap{
	
	public static final byte BIT_FLIP_NONE = 0x00;
	public static final byte BIT_FLIP_X = 0x01;
	public static final byte BIT_FLIP_Y = 0x10;
	public static final byte BIT_FLIP_BOTH = 0x11;
	
	private int xOffset, yOffset;
	
	/**
	 * Instantiates the screen.
	 * @param width Width of the screen.
	 * @param height Height of the screen.
	 */
	public Screen(int width, int height){
		super(width, height);
	}
	
	public void drawMap(Bitmap map, int xPos, int yPos){
		drawMap(map, xPos, yPos, BIT_FLIP_NONE, true);
	}
	
	public void drawMap(Bitmap map, int xPos, int yPos, byte flip){
		drawMap(map, xPos, yPos, flip, true);
	}
	
	public void drawMap(Bitmap map, int xPos, int yPos, boolean respectOffset){
		drawMap(map, xPos, yPos, BIT_FLIP_NONE, respectOffset);
	}
	
	/**
	 * Draw a bitmap onto the screen.
	 * @param map The bitmap to draw.
	 * @param xPos The x position of the bitmap on the screen.
	 * @param yPos The y position of the bitmap on the screen.
	 * @param flip How to flip the bitmap vertically or horizontally.
	 * @param respectOffset Whether or not the bitmap should respect the screen's offset.
	 */
	public void drawMap(Bitmap map, int xPos, int yPos, byte flip, boolean respectOffset){
		boolean flipX = (flip == BIT_FLIP_X) || (flip == BIT_FLIP_BOTH);
		boolean flipY = (flip == BIT_FLIP_Y) || (flip == BIT_FLIP_BOTH);
		
		if(respectOffset){
			xPos -= xOffset;
			yPos -= yOffset;
		}
		
		for(int y = 0; y < map.getHeight() * map.getScale(); y++){
			int yy = flipY ? yPos + (map.getHeight() - y) : yPos + y;
			for(int x = 0; x < map.getWidth() * map.getScale(); x++){
				int xx = flipX ? xPos + (map.getWidth() - x) : xPos + x;
				
				if(xx < 0 || xx >= width || yy < 0 || yy >= height) continue;
				
				int col = map.pixels[(x / map.getScale()) + (y / map.getScale()) * map.getWidth()];
				if(col != transparentColor){
					pixels[xx + yy * width] = col;
				}
			}
		}
	}
	
	/**
	 * Set the offset of the screen.
	 * @param xo The x offset.
	 * @param yo The y offset.
	 */
	public void setOffset(int xo, int yo){
		xOffset = xo;
		yOffset = yo;
	}
	
	/**
	 * Clear the screen with its fill color
	 */
	public void clear(){
		for(int i = 0; i < pixels.length; i++){
			pixels[i] = fillColor;
		}
	}
}