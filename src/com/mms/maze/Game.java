package com.mms.maze;

import com.mms.maze.entity.Player;
import com.mms.maze.graphics.Bitmap;
import com.mms.maze.graphics.Screen;
import com.mms.maze.graphics.Text;
import com.mms.maze.gui.AboutMenu;
import com.mms.maze.gui.ControlsMenu;
import com.mms.maze.gui.LoadMenu;
import com.mms.maze.gui.MainMenu;
import com.mms.maze.gui.Menu;
import com.mms.maze.gui.PauseMenu;
import com.mms.maze.gui.WinAlert;
import com.mms.maze.level.LevelHandler;

public class Game {
	
	//game handling
	private Container container;
	private GameState state;
	private Screen screen;
	private LevelHandler levelHandler;
	private Player player;
	
	//saving
	private Save save;
	public int saveNum = -1;
	
	//menus
	private boolean finishedMenuLoading = false;
	private Menu currentMenu;
	public Menu mainMenu;
	public Menu aboutMenu;
	public Menu controlsMenu;
	public PauseMenu pauseMenu;
	public LoadMenu loadMenu;
	public WinAlert winAlert;
	
	//time
	private double pauseTime = 0;
	public int time = 0;
	
	/**
	 * Instantiate the screen.
	 * @param screen The screen that the game should render to.
	 */
	public Game(Container container, Screen screen){
		this.container = container;
		this.screen = screen;
		levelHandler = new LevelHandler(this);
		player = new Player(container.getInput());
		levelHandler.loadLevel("hub");
		save = new Save(this);
	}
	
	/**
	 * Load the game's menus
	 * @return If the menus are done loading
	 */
	private boolean loadMenus(){
		if(mainMenu == null){
			mainMenu = new MainMenu(this);
			currentMenu = mainMenu;
			return false;
		}else if(aboutMenu == null){
			aboutMenu = new AboutMenu(this);
			return false;
		}else if(controlsMenu == null){
			controlsMenu = new ControlsMenu(this);
			return false;
		}else if(pauseMenu == null){
			pauseMenu = new PauseMenu(this);
			return false;
		}else if(loadMenu == null){
			loadMenu = new LoadMenu(this);
			return false;
		}else if(winAlert == null){
			winAlert = new WinAlert(this);
			return false;
		}else{
			return true;
		}
	}
	
	/**
	 * Sets the state of the game
	 * @param state
	 */
	public void setState(GameState state){
		this.state = state;
	}
	
	/**
	 * Gets the state of the game
	 * @return
	 */
	public GameState getState(){
		return state;
	}
	
	/**
	 * Win the game
	 */
	public void win(){
		//player.alreadyWon = true;
		player.progression = 1;
		player.direction = 0;
		winAlert.updateTime();
		time = 0;
		save.saveGame();
		state = GameState.WIN;
	}
	
	/**
	 * Pause the game.
	 * @param seconds Seconds to pause.
	 */
	public void pause(double seconds){
		this.pauseTime = seconds * 60;
	}
	
	public Bitmap lastImage = new Bitmap(240, 180, 0);
	
	/**
	 * Game logic.
	 */
	public void tick(){
		//don't run if the game is paused
		if(pauseTime > 0){
			pauseTime--;
			return;
		}
		
		switch(state){
		case STARTING:
			if(!finishedMenuLoading){
				finishedMenuLoading = loadMenus();
				return;
			}else{
				state = GameState.IN_MENU;
			}
			break;
		case IN_MENU:
			currentMenu.tick();
			break;
		case PLAYING:
			levelHandler.getCurrentLevel().tick();
			player.tick();
			if(getInput().isKeyPressed(Controls.PAUSE)){
				pauseMenu.updateBackground();
				state = GameState.PAUSED;
				pause(0.3);
			}
			time++;
			break;
		case PAUSED:
			pauseMenu.tick();
			if(getInput().isKeyPressed(Controls.PAUSE)){
				state = GameState.PLAYING;
				pause(0.3);
			}
			break;
		case WIN:
			winAlert.tick();
		}
	}
	
	Bitmap testMap = new Bitmap("/tex/test.png", 0, 0, 16);
	/**
	 * Game drawing.
	 */
	public void render(){
		/* render engine testing
		testMap.setColors(0x7f007f, 0xffffff, 0xF823ED, 0xff00ff);
		testMap.setScale(2);
		screen.drawMap(testMap, 100, 100);
		Text.addToTextQueue(new Text("Text test", 100, 40, 0xffffff, 24));*/
		screen.clear();
		switch(state){
		case STARTING:
			screen.drawMap(new Bitmap("/tex/gui/title.png"), 0, 0);
			Text.addToTextQueue(new Text("Loading game...", 40, 60, 0xffffff, 24));
			break;
		case IN_MENU:
			currentMenu.render(screen);
			break;
		case PLAYING:
			levelHandler.getCurrentLevel().render(screen, player.getX(), player.getY());
			player.render(screen);
			updateLastImage();
			break;
		case PAUSED:
			pauseMenu.render(screen);
			break;
		case WIN:
			winAlert.render(screen);
			break;
		}
	}
	
	/**
	 * Update the last image, used for menu backgrounds
	 */
	private void updateLastImage(){
		for(int i = 0; i < lastImage.pixels.length; i++){
			lastImage.pixels[i] = screen.pixels[i];
		}
	}
	
	/**
	 * Gets width of game screen
	 * @return width
	 */
	public int getWidth(){
		return screen.getWidth();
	}
	
	/**
	 * Gets height of game screen
	 * @return height
	 */
	public int getHeight(){
		return screen.getHeight();
	}
	
	/**
	 * Gets game's input source
	 * @return input
	 */
	public Input getInput(){
		return container.getInput();
	}
	
	/**
	 * Sets current menu of the game
	 * @param menu
	 */
	public void setCurrentMenu(Menu menu){
		currentMenu = menu;
	}
	
	/**
	 * Gets the game's player
	 * @return
	 */
	public Player getPlayer(){
		return player;
	}
	
	/**
	 * Gets the game's level handler
	 * @return
	 */
	public LevelHandler getLevelHandler(){
		return levelHandler;
	}
	
	/**
	 * Gets the game's screen
	 * @return
	 */
	public Screen getScreen(){
		return screen;
	}
	
	/**
	 * Gets the game's container
	 * @return
	 */
	public Container getContainer(){
		return container;
	}
	
	/**
	 * Gets the game's save
	 * @return
	 */
	public Save getSave(){
		return save;
	}
	
	public enum GameState {
		STARTING,
		IN_MENU,
		PLAYING,
		PAUSED,
		WIN
	}
}